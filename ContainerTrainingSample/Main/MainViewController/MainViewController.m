//
//  MainViewController.m
//  ContainerTrainingSample
//
//  Created by Vladyslav Bedro on 10/3/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UIView*             dataContainer;
@property (weak, nonatomic) IBOutlet UISegmentedControl* navigationSegmentedControl;

//properties
@property (strong, nonatomic) UIViewController* containerViewController;

@end

@implementation MainViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Actions -

- (IBAction) onNavSegmentedControlPressed: (UISegmentedControl*) sender
{
    UIViewController* viewController = nil;
    
    UIStoryboard* storyBoard;
    
    switch (self.navigationSegmentedControl.selectedSegmentIndex)
    {
        case 0:
            [self resetContainerView];
            
            storyBoard = [UIStoryboard storyboardWithName: @"AScreen"
                                                   bundle: [NSBundle mainBundle]];
            
            viewController = [storyBoard instantiateViewControllerWithIdentifier: @"AScreenID"];
            break;
            
        case 1:
            [self resetContainerView];
            
            storyBoard = [UIStoryboard storyboardWithName: @"BScreen"
                                                   bundle: [NSBundle mainBundle]];
            
            viewController = [storyBoard instantiateViewControllerWithIdentifier: @"BScreenID"];
            break;
            
        case 2:
            [self resetContainerView];
            
            storyBoard = [UIStoryboard storyboardWithName: @"CScreen"
                                                   bundle: [NSBundle mainBundle]];
            
            viewController = [storyBoard instantiateViewControllerWithIdentifier: @"CScreenID"];
            
        default:
            break;
    }
    
    [self addChildViewController: viewController];
    
    
    viewController.view.frame = CGRectMake(0, 0,
                               self.dataContainer.frame.size.width,
                               self.dataContainer.frame.size.height);
    
    
    [self.dataContainer addSubview: viewController.view];
    
    [viewController didMoveToParentViewController: self];
    
    self.containerViewController = viewController;
}


#pragma mark - Internal methods -

- (void) resetContainerView
{
    [self.containerViewController willMoveToParentViewController: nil];
    
    [self.containerViewController.view removeFromSuperview];
    
    [self.containerViewController removeFromParentViewController];
    
    self.containerViewController = nil;
}

@end
