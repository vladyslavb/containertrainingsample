//
//  MainViewController.h
//  ContainerTrainingSample
//
//  Created by Vladyslav Bedro on 10/3/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
