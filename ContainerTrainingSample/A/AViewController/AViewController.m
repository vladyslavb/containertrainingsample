//
//  AViewController.m
//  ContainerTrainingSample
//
//  Created by Vladyslav Bedro on 10/3/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AViewController.h"

@interface AViewController ()

@end

@implementation AViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}

@end
