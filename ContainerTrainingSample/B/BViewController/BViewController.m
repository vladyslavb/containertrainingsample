//
//  BViewController.m
//  ContainerTrainingSample
//
//  Created by Vladyslav Bedro on 10/3/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BViewController.h"

@interface BViewController ()

@end

@implementation BViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}

@end
